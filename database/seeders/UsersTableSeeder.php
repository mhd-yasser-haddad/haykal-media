<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Setting;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1, 
            'role_id' => 1, 
            'name' => 'Yasser', 
            'email' => 'yasserhaddad99@gmail.com', 
            'avatar' => 'users/default.png', 
            'email_verified_at' => NULL, 
            'password' => '$2y$10$yDyBuFPZKKzxxLrjUC06yOZNSnpsDG3elA3/GKJMbmriQMHEoD1p.',
            'remember_token' => NULL,
            'settings' => NULL,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);

    }
}
