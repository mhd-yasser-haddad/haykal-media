<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        DB::insert("INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
            (1, 'admin', 'Administrator', '2020-10-30 08:39:17', '2020-10-30 08:39:17'),
            (2, 'user', 'Normal User', '2020-10-30 08:39:17', '2020-10-30 08:39:17'),
            (3, 'editor', 'Editor', '2020-10-31 12:10:18', '2020-10-31 12:10:18');
        ");
    }
}
