<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        DB::insert("INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
            (1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
            (2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
            (3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
            (4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
            (5, 'admin.bg_image', 'Admin Background Image', 'settings\\\\October2020\\\\LLYJqjBO2m7dyH0sIepW.jpg', '', 'image', 5, 'Admin'),
            (6, 'admin.title', 'Admin Title', 'Articles', '', 'text', 1, 'Admin'),
            (7, 'admin.description', 'Admin Description', 'Welcome to articles', '', 'text', 2, 'Admin'),
            (8, 'admin.loader', 'Admin Loader', 'settings\\\\October2020\\\\ZM5Bw1aQDOEhOfOzVu3Z.png', '', 'image', 3, 'Admin'),
            (9, 'admin.icon_image', 'Admin Icon Image', 'settings\\\\October2020\\\\zLaLwe3gnrDNrX3Ucyag.png', '', 'image', 4, 'Admin'),
            (10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');  
        ");
    }
}
