<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class MenuItemsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
            (1, 1, 'Dashboard', '', '_self', 'voyager-home', '#000000', NULL, 1, '2020-10-30 08:39:17', '2020-10-30 09:05:41', 'voyager.dashboard', 'null'),
            (2, 1, 'Media', '', '_self', 'voyager-images', NULL, 5, 1, '2020-10-30 08:39:17', '2020-10-30 09:04:27', 'voyager.media.index', NULL),
            (3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 5, '2020-10-30 08:39:17', '2020-10-30 09:05:05', 'voyager.users.index', NULL),
            (4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 6, '2020-10-30 08:39:17', '2020-10-30 09:05:06', 'voyager.roles.index', NULL),
            (5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 7, '2020-10-30 08:39:17', '2020-10-30 09:05:06', NULL, NULL),
            (6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 2, '2020-10-30 08:39:17', '2020-10-30 09:05:06', 'voyager.menus.index', NULL),
            (7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 3, '2020-10-30 08:39:17', '2020-10-30 09:05:06', 'voyager.database.index', NULL),
            (8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 4, '2020-10-30 08:39:17', '2020-10-30 09:05:06', 'voyager.compass.index', NULL),
            (9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 5, '2020-10-30 08:39:17', '2020-10-30 09:05:06', 'voyager.bread.index', NULL),
            (10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, 5, 7, '2020-10-30 08:39:17', '2020-10-30 09:05:06', 'voyager.settings.index', NULL),
            (11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 6, '2020-10-30 08:39:18', '2020-10-30 09:05:06', 'voyager.hooks', NULL),
            (12, 1, 'Articles', '', '_self', 'voyager-receipt', NULL, NULL, 2, '2020-10-30 08:58:46', '2020-10-30 09:04:50', 'voyager.articles.index', NULL),
            (13, 1, 'Categories', '', '_self', 'voyager-dot-3', NULL, NULL, 3, '2020-10-30 09:00:39', '2020-10-30 09:04:59', 'voyager.categories.index', NULL),
            (14, 1, 'Tags', '', '_self', 'voyager-tag', '#000000', NULL, 4, '2020-10-30 09:01:34', '2020-10-30 09:05:22', 'voyager.tags.index', 'null'); 
        ");
    }
}
