<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;

class MenusTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
            (1, 'admin', '2020-10-30 08:39:17', '2020-10-30 08:39:17');
        ");
    }
}
