<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class DataTypesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        DB::insert("INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
            (1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\\\Voyager\\\\Models\\\\User', 'TCG\\\\Voyager\\\\Policies\\\\UserPolicy', 'TCG\\\\Voyager\\\\Http\\\\Controllers\\\\VoyagerUserController', '', 1, 0, NULL, '2020-10-30 08:39:17', '2020-10-30 08:39:17'),
            (2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\\\Voyager\\\\Models\\\\Menu', NULL, '', '', 1, 0, NULL, '2020-10-30 08:39:17', '2020-10-30 08:39:17'),
            (3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\\\Voyager\\\\Models\\\\Role', NULL, 'TCG\\\\Voyager\\\\Http\\\\Controllers\\\\VoyagerRoleController', '', 1, 0, NULL, '2020-10-30 08:39:17', '2020-10-30 08:39:17'),
            (10, 'articles', 'articles', 'Article', 'Articles', 'voyager-receipt', 'App\\\\Models\\\\Article', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-30 08:58:46', '2020-10-30 09:03:46'),
            (11, 'categories', 'categories', 'Category', 'Categories', 'voyager-dot-3', 'App\\\\Models\\\\Category', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-10-30 09:00:39', '2020-10-30 09:00:39'),
            (12, 'tags', 'tags', 'Tag', 'Tags', 'voyager-tag', 'App\\\\Models\\\\Tag', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-10-30 09:01:34', '2020-10-30 09:04:06');        
        ");
    }
}
