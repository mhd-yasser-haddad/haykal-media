<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
            (1, 1),
            (1, 3),
            (2, 1),
            (3, 1),
            (4, 1),
            (5, 1),
            (6, 1),
            (7, 1),
            (8, 1),
            (9, 1),
            (10, 1),
            (11, 1),
            (12, 1),
            (13, 1),
            (14, 1),
            (15, 1),
            (16, 1),
            (17, 1),
            (18, 1),
            (19, 1),
            (20, 1),
            (21, 1),
            (22, 1),
            (23, 1),
            (24, 1),
            (25, 1),
            (26, 1),
            (27, 1),
            (27, 3),
            (28, 1),
            (28, 3),
            (29, 1),
            (29, 3),
            (30, 1),
            (30, 3),
            (31, 1),
            (31, 3),
            (32, 1),
            (33, 1),
            (34, 1),
            (35, 1),
            (36, 1),
            (37, 1),
            (38, 1),
            (39, 1),
            (40, 1),
            (41, 1);
        ");
    }
}
