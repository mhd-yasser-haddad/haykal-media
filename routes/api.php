<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\ArticleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'article'], function () {

    Route::get('/', [ArticleController::class, 'getArticles']);
    Route::get('/{id}', [ArticleController::class, 'getArticle']);

    Route::middleware('auth:api')->group(function () {
        Route::post('/create', [ArticleController::class, 'createArticle']);
        Route::post('/{id}/update', [ArticleController::class, 'updateArticle']);
        Route::post('/{id}/delete', [ArticleController::class, 'deleteArticle']);
    });
});

Route::post('/login', [UserController::class, 'login']);
Route::post('/register', [UserController::class, 'register']);

Route::get('/getTags', [ArticleController::class, 'getTags']);
Route::get('/getCategories', [ArticleController::class, 'getCategories']);
