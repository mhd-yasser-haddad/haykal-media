<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('website.master');
})->name('home');

Route::get('/create', function () {
    return view('website.article.create');
});

Route::get('/login', function(){
    return view('website.user.login');
})->name('login');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


