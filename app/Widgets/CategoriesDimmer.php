<?php

namespace App\Widgets;

use App\Models\Category;

use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;
use Illuminate\Support\Facades\Auth;

class CategoriesDimmer extends BaseDimmer
{
    
    protected $config = [];

    
    public function run()
    {
        $count = Category::count();
        $string = __('dimmers.categories');

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-dot-3',
            'title'  => "{$count} {$string}",
            'text'   => __('dimmers.categoryText'),
            'button' => [
                'text' => __('dimmers.category'),
                'link' => route('voyager.categories.index'),
            ],
            'image' => asset('img/dimmers/categories.jpg')
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        //return true;
        return Auth::user()->hasPermission('browse_categories');
    }
}