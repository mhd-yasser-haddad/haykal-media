<?php

namespace App\Widgets;

use App\Models\Tag;

use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;
use Illuminate\Support\Facades\Auth;

class TagsDimmer extends BaseDimmer
{
    
    protected $config = [];

    
    public function run()
    {
        $count = Tag::count();
        $string = __('dimmers.tags');

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-tag',
            'title'  => "{$count} {$string}",
            'text'   => __('dimmers.tagText'),
            'button' => [
                'text' => __('dimmers.tag'),
                'link' => route('voyager.tags.index'),
            ],
            'image' => asset('img/dimmers/tags.jpg')
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        //return true;
        return Auth::user()->hasPermission('browse_tags');
    }
}