<?php

namespace App\Widgets;

use App\Models\Article;

use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;
use Illuminate\Support\Facades\Auth;

class ArticlesDimmer extends BaseDimmer
{
    
    protected $config = [];

    
    public function run()
    {
        $count = Article::count();
        $string = __('dimmers.articles');

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-receipt',
            'title'  => "{$count} {$string}",
            'text'   => __('dimmers.articleText'),
            'button' => [
                'text' => __('dimmers.article'),
                'link' => route('voyager.articles.index'),
            ],
            'image' => asset('img/dimmers/articles.jpg')
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        //return true;
        return Auth::user()->hasPermission('browse_articles');
    }
}