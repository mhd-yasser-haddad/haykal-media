<?php

namespace App\Http\Controllers;

use App\APIException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function processRequestWrapper($processRequestFunction)
    {
        try {
            $returnedValue = $processRequestFunction();
            if ($returnedValue == NULL) {
                $returnedValue = [];
            }
            return $this->sendResponse($returnedValue);
        } catch (APIException $th) {
            return $this->sendError($th->getMessage(), $th->getCode());
        }
    }

    public function sendResponse($result = null, $message = '', $view = null, $statusCode = 200)
    {
        $response = [
            'data' => $result,
            'message' => $message,
        ];
        if (request()->wantsJson()) {
            return response()->json($response, $statusCode);
        }else{   
            return view($view)->with('data',(object)$response);
        }
    }

    public function sendError($message = '',$view = null, $statusCode = 400)
    {
        $response = [
            'data' => null,
            'message' => $message,
        ];
        if (request()->wantsJson()) {
            return response()->json($response, $statusCode);
        }else {
            return view($view)->with('data',(object)$response);
        }
    }
}
