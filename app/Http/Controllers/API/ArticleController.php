<?php

namespace App\Http\Controllers\API;

use App\Models\Tag;
use App\Models\Article;
use App\Models\Category;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

class ArticleController extends BaseController
{
    public function getTags(Request $request)
    {
        $tags = Tag::get();
        return SELF::sendResponse($tags, 'Data retrieved successfully', '', 200);
    }

    public function getCategories(Request $request)
    {
        $categories = Category::get();
        return SELF::sendResponse($categories, 'Data retrieved successfully', '', 200);
    }

    public function getArticles(Request $request)
    {
        $articles = Article::with(['user', 'tags', 'categories'])->get();
        return SELF::sendResponse($articles, 'Data retrieved successfully', 'index', 200);
    }

    public function getArticle(Request $request, $id)
    {
        $article = Article::with(['user', 'tags', 'categories'])->find($id);
        if(!$article)
        {
            return SELF::sendError('Article not found', 'index', 404);
        }
        return SELF::sendResponse($article, 'Data retrived successfully', 'detail', 200);
    }

    public function createArticle(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'         => 'required|string',
            'content'       => 'required|string',
            'image'         => 'required|mimes:jpeg,png,jpg',
            'tags'          => 'required|array',
            'categories'    => 'required|array',
            'tags.*'        => 'exists:tags,id',
            'categories.*'  => 'exists:categories,id'
        ]);
        if ($validator->fails()) 
        {
            return Self::sendError($validator->messages()->first(), null, 404);
        }

        // Getting th user
        $user = Auth::user();

        // Storing the image
        $extension = $request->image->extension();
        $imageName = $request->image->getClientOriginalName();
        $imagePath = '/public/articles/'.$user->name;
        $request->image->storeAs($imagePath, $imageName);

        // Inserting the article
        $article = new Article();
        $article->title = $request->title;
        $article->content = $request->content;
        $article->image = '/articles/'.$user->name.'/'.$imageName;
        $article->user_id = $user->id;

        if ($article->save()) 
        {
            $tagIDs = $request->tags;
            $categoriesIDs = $request->categories;
            $article->tags()->sync($tagIDs);
            $article->categories()->sync($categoriesIDs);
        }
        else
        {
            return Self::sendError('Something went wrong, please try again', null, 400);
        }

        return SELF::sendResponse($article, 'Article inserted successfully', 'Detail', 201);
    }

    public function updateArticle(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title'         => 'required|string',
            'content'       => 'required|string',
            'image'         => 'required|mimes:jpeg,png,jpg',
            'tags'          => 'required|array',
            'categories'    => 'required|array',
            'tags.*'        => 'exists:tags,id',
            'categories.*'  => 'exists:categories,id'
        ]);
        if ($validator->fails()) 
        {
            return $this->sendError($validator->messages()->first(), null, 400);
        }

        // Getting th user
        $user = Auth::user();

        // Getting the article
        $article = Article::find($id);
        if(!$article)
        {
            return SELF::sendError('Article not found', 'index', 404);
        }

        if($article->user_id != $user->id)
        {
            return SELF::sendError('You cannot update the article', 'index', 401);
        }

        // Storing the image
        $extension = $request->image->extension();
        $imageName = $request->image->getClientOriginalName();
        $imagePath = '/public/articles/'.$user->name;
        $request->image->storeAs($imagePath, $imageName);

        // Updting the article
        $article->title = $request->title;
        $article->content = $request->content;
        $article->image = '/storage/articles/'.$user->name.'/'.$imageName;
        $article->user_id = $user->id;

        if ($article->save()) 
        {
            $tagIDs = $request->tags;
            $categoriesIDs = $request->categories;
            $article->tags()->sync($tagIDs);
            $article->categories()->sync($categoriesIDs);
            $success = true;
        }

        return SELF::sendResponse($article, 'Article updated successfully', 'Detail', 200);
    }

    public function deleteArticle(Request $request, $id)
    {
        // Getting th user
        $user = Auth::user();

        // Getting the article
        $article = Article::find($id);
        if(!$article)
        {
            return SELF::sendError('Article not found', 'index', 404);
        }

        if($article->user_id != $user->id)
        {
            return SELF::sendError('You cannot delete the article', 'index', 401);
        }
        
        $article->delete();
        return SELF::sendResponse(null, 'Article deleted successfully', 'Detail', 200);
    }
}
