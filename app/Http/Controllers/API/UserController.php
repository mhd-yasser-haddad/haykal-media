<?php

namespace App\Http\Controllers\API;

use App\Models\User;

use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

class UserController extends BaseController
{
    public function Login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'     => 'required|email',
            'password'  => 'required|min:8'
        ]);
        if ($validator->fails()) 
        {
            return $this->sendError($validator->messages()->first(), null, 404);
        }

        $user = User::FindByEmail($request->email)->first();
        if (!empty($user) && \Hash::check($request->password, $user->password))
        {
                $token = $user->createToken('');
                $data = [
                    'access_token'  =>$token->accessToken,
                    'user'          =>$user
                ];
                return $this->sendResponse($data ,'Logged in successfully');
        }
        return $this->sendError('Invalid email or password', null, 404);
    }

    public function Register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'              => 'required|string',
            'email'             => 'required|email|unique:users,email',
            'password'          => 'required|min:8',
            'confirm_password'  => 'required|same:password',
            'avatar'            => 'required|mimes:jpeg,png,jpg',
        ]);
        if ($validator->fails()) 
        {
            return $this->sendError($validator->messages()->first(), null, 404);
        }

        // Storing the image
        $extension = $request->avatar->extension();
        $imageName = $request->avatar->getClientOriginalName();
        $imagePath = '/public/users/'.$request->name;
        $request->avatar->storeAs($imagePath, $imageName);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role_id = 2;
        $user->avatar = '/users/'.$request->name.'/'.$imageName;
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();

        if($user->save())
        {
            return SELF::sendResponse($user, 'User created successfully', 'Detail', 201);
        }
        Self::sendError('Something went wrong, please try again', null, 400);
    }
}
