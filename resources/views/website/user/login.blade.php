@extends('website.master')

@section('css')
<style type="text/css" href="{{asset('css/user/login.css')}}"></style>
@endsection

@section('content')

<form class="form-signin" method="post">
    @csrf
    <div class="d-flex flex-column pb-3">
        <img class="img-fluid mx-auto d-block" src="{{asset("img/website/logo.png")}}" alt="login logo" width="72" height="72">
    </div>

    <h1 class="h3 mb-3 font-weight-normal">Login</h1>

    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>

    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>

    @if(isset($data['message']) && $data['message'] != '')
        <div style="color: red">
            <p>{{$data['message']}}</p>
        </div>
    @endif

    <button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>

</form>
@endsection


