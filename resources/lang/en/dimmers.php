<?php

return [
    'article'       => 'Article',
    'articles'      => 'Articles',
    'articleText'   => 'Checkout all the articles',

    'tag'           => 'Tags',
    'tags'          => 'Tags',
    'tagText'       => 'Checkout all the tags',

    'category'      => 'Category',
    'categories'    => 'Categories',
    'categoryText'  => 'Checkout all the categories'

];
